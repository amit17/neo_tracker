var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bcSchema = new Schema({
  net: {
    type: String,
    required: true,
    unique: true,
    default : 'privnet_net'
  },
  currentBlock: {type : Number, default : 0}
})

module.exports = mongoose.model('BC', bcSchema);
