var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  name: {
      type:String,
      required: true
  },
  email: {
      type:String,
      required: true,
      unique: true
  },
  password: String,
  currentBlock: {type : Number, default : 1}, /* change the default value to something more relevent */
},
{
    timestamps: true
});

function findUserByID(User, id) {
  return User.findById(id, { password: 0, wallets: 0, deployed: 0});
}

function findUserByEmail(User, _email) {
  return User.findOne({ email: _email }, { wallets: 0, deployed: 0});
}

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');
module.exports.findUserByID = findUserByID;
module.exports.findUserByEmail = findUserByEmail;
