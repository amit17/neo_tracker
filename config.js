var jwt = {
  'secret': 'supersecret',
  'token_expire_time': 123600
}

var db = {
  //MONGOOSE_CONNECTION_STRING : 'mongodb+srv://neo_dex:neo_dex@neo-lam8l.mongodb.net/',
  MONGOOSE_CONNECTION_STRING : 'mongodb+srv://neo_testnet:eKqcjPdH632Mi8r@neotestnet-ctbc7.mongodb.net/',
  MONGOOSE_DBNAME : 'neo_testnet',
  AGENDA_CONNECTION_STRING : 'mongodb+srv://neo_testnet:eKqcjPdH632Mi8r@neotestnet-ctbc7.mongodb.net/',
  AGENGA_DBNAME : 'neo_testnet',
  AGENGA_COLLECTION_NAME : 'agendaJobs'
}

var blockchain = {
  RPC_URL : "http://127.0.0.1:20332/",
  CHAIN_ID : '0x03',
  CONTRACT_ADDRESS : "0x4b6dc098507569b853267a881faaefe15e84c852"
}

var webHookUrls = {
  PING_URL : 'https://hwuxiib9b3.execute-api.us-west-2.amazonaws.com/Prod/Orders'
  // PING_URL : 'http://192.168.20.92:50703/api/WandAPI'
}

module.exports = {
  jwtConfig : jwt,
  dbConfig : db,
  blockchainConfig : blockchain,
  webhookConfig : webHookUrls
}
