var Agenda = require('agenda');
var fs = require('fs')
var path = require('path')
var queues = require('./queues');
const MongoClient = require('mongodb').MongoClient;
var isWorker = process.env.WORKER ? true : false
var JOBS_FOLDER = './jobs'
var dbConfig = require('./config').dbConfig

var agenda = new Agenda();
var options = {autoReconnect: true, reconnectTries: Number.MAX_SAFE_INTEGER, reconnectInterval: 5000}
MongoClient.connect(dbConfig.AGENDA_CONNECTION_STRING, options, (error, client) => {
	// Note : Mongodb > 3.00 -- https://stackoverflow.com/a/47694265/3050426

	var db = client.db(dbConfig.AGENGA_DBNAME)
	agenda.mongo(db, dbConfig.AGENGA_COLLECTION_NAME, function() {})
	agenda.on('ready', function() {
		// If worker define jobs and start if jobs are present
		var jobs = []
		if (isWorker) {
			var files = fs.readdirSync(JOBS_FOLDER)
			if (files && files.length) {
				files.forEach(function(file) {
					var pathObj = path.parse(file)
					// require(path.join(JOBS_FOLDER, pathObj.name))(agenda)
					if (pathObj.name == "logs") {
						console.log(pathObj);
						jobs.push(JOBS_FOLDER + '/' + pathObj.name)
					}
					require(JOBS_FOLDER + '/' + pathObj.name)(agenda)
				})
				require('./db')
				global.__root   = __dirname + '/';
				console.log('Worker Jobs started:', jobs)
				agenda.start()
			}
		}
		console.log('Agenda is ready')
	})
	function graceful() {
	  agenda.stop(function() {
	    process.exit(0);
	  });
	}

	process.on('SIGTERM', graceful);
	process.on('SIGINT' , graceful);

})

module.exports = agenda;
