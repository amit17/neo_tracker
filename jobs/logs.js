const User = require('../models/users');
const queues = require('../queues');
const request = require('request');
const superagent = require('superagent');
const async_lib = require('async')
const BigNumber = require('bignumber.js')
var AWS = require('aws-sdk');
const Neon = require("@cityofzion/neon-js");
const blockchainConfig = require('../config').blockchainConfig;
const webhookConfig = require('../config').webhookConfig;
const contract_address = blockchainConfig.CONTRACT_ADDRESS;
const RPC_URL = blockchainConfig.RPC_URL;

// Initialize the Amazon Cognito credentials provider withdrawRequestTestNet
AWS.config.region = 'us-east-2'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: 'us-east-2:b1717ce2-83a1-481a-9070-7fd2d0da1468',
});

// call lambda to set mark done and send the withdraw request
function setMarkDoneLambda(withdrawingAddr, assetId, amount) {
  var lambda = new AWS.Lambda({region: 'us-east-2', apiVersion: '2015-03-31'});

  let payload  = {
    "wallet_address": Neon.wallet.getAddressFromScriptHash(Neon.u.reverseHex(withdrawingAddr)),
    "token_address": Neon.u.reverseHex(assetId),
    "amount": amount,
    "withdraw_stage": "51",
    "withdraw_from": "53" // TODO get this also from the event
  };

  // create JSON object for parameters for invoking Lambda function
  var pullParams = {
    FunctionName : 'processWithdrawTestNet',
    InvocationType : 'RequestResponse',
    LogType : 'None',
    Payload: JSON.stringify(payload)
  };

  lambda.invoke(pullParams, function(error, data) {
    console.log(error, data);
    if (error) {
      prompt(error);
    } else {
      pullResults = JSON.parse(data.Payload);
      console.log(pullResults);
    }
  });
}

// call lambda to set mark done and send the withdraw request
function setWithdrawDoneLambda(withdrawingAddr, assetId, amount) {
  var lambda = new AWS.Lambda({region: 'us-east-2', apiVersion: '2015-03-31'});

  let payload  = {
    "wallet_address": Neon.wallet.getAddressFromScriptHash(Neon.u.reverseHex(withdrawingAddr)),
    "token_address": Neon.u.reverseHex(assetId),
    "amount": amount,
    "withdraw_done": 1,
  };

  // create JSON object for parameters for invoking Lambda function
  var pullParams = {
    FunctionName : 'processWithdrawTestNet',
    InvocationType : 'RequestResponse',
    LogType : 'None',
    Payload: JSON.stringify(payload)
  };

  lambda.invoke(pullParams, function(error, data) {
    console.log(error, data);
    if (error) {
      prompt(error);
    } else {
      pullResults = JSON.parse(data.Payload);
      console.log(pullResults);
    }
  });
}

longToByteArray = function( /*long*/ long) {
  // we want to represent the input as a 8-bytes array
  var byteArray = [0, 0, 0, 0, 0, 0, 0, 0];
  for (var index = 0; index < byteArray.length; index++) {
    var byte = long & 0xff;
    byteArray[index] = byte;
    long = (long - byte) / 256;
  }
  return byteArray;
};

byteArrayToLong = function( /*byte[]*/ byteArray) {
  var value = 0;
  for (var i = byteArray.length - 1; i >= 0; i--) {
    value = (value * 256) + byteArray[i];
  }
  return value;
};

async function updateUserCurrentBlock(userId, latest_block) {
  if (!userId || !latest_block)
    return false
  return User.findByIdAndUpdate(userId, {
    $set: {
      currentBlock: latest_block
    }
  }, function(err) {
    //console.log('User currentBlock Updated')
  })
}

function hex2a(hexx) {
  var hex = hexx.toString(); //force conversion
  var str = '';
  for (var i = 0;
    (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
  return str;
}

function getBlockNumber() {
  var options = {
    method: 'POST',
    url: RPC_URL,
    headers: {
      'cache-control': 'no-cache'
    },
    body: '{\n   "jsonrpc": "2.0",\n   "method": "getblockcount",\n   "params":[],\n   "id": 1\n}'
  };

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error);
      if (response.statusCode != 200) {
        return reject('Invalid status code <' + response.statusCode + '>');
      }
      return resolve(JSON.parse(body).result);
    });
  })
}

function getTxs(blockNumber) {
  var options = {
    method: 'POST',
    url: RPC_URL,
    headers: {
      'cache-control': 'no-cache'
    },
    body: '{\n  "jsonrpc": "2.0",\n  "method": "getblock",\n  "params": [' + blockNumber + ' , 1],\n  "id": 1\n}'
  };

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error);
      if (response.statusCode != 200) {
        return reject('Invalid status code <' + response.statusCode + '>');
      }
      // console.log("body",body);
      body = JSON.parse(body);
      if (body.result) {
        return resolve(body.result.tx);
      } else {
        return resolve(body.error);
      }
    });
  });
}

function getLogs(txid) {
  var options = {
    method: 'POST',
    url: RPC_URL,
    headers: {
      'cache-control': 'no-cache'
    },
    body: '{\n  "jsonrpc": "2.0",\n  "method": "getapplicationlog",\n  "params": ["' + txid + '"],\n  "id": 1\n}'
  };

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error);
      if (response.statusCode != 200) {
        return reject('Invalid status code <' + response.statusCode + '>');
      }
      return resolve(JSON.parse(body));
    });
  });
}

function processTransactions(currentBlock, txs) {
  return new Promise(async (resolve, reject) => {
    let interestedLogs = []
    // console.log(txs);
    for (let i = 0; i < txs.length; i++) {
      // console.log("tx", txs[i]);
      if (txs[i].type == "InvocationTransaction") {
        // console.log("tx", txs[i]);
        let log = await getLogs(txs[i].txid);
        if (log.result) {
          if (log.result.vmstate == "HALT, BREAK") {
            let interestedLog = parseLog(currentBlock, i, log.result);
            // console.log("log", interestedLog);
            if (interestedLog.length > 0) {
              interestedLogs.push(interestedLog);
            }
          }
        }
      }
    }
    // console.log("interestedLogs1", interestedLogs);
    // if (error) return reject(error);
    return resolve({
      interestedLogs
    });
  });
}

function parseLog(block, index, result) {
  // console.log("parl", notifications);
  let interestedLogs = [];
  let notifications = result.notifications;
  // console.log("notifications", notifications);
  for (i = 0; i < notifications.length; i++) {
    // console.log("notifications", notifications[i].contract);
    if (notifications[i].contract == contract_address && notifications[i].state.type == "Array") {
      notification = notifications[i].state.value;
      // console.log("notification", notification);
      let basket_amounts;
      var event = {};
      switch (hex2a(notification[0].value)) {
        case "initialized":
          console.log("initialized");
          event.eventName = "initialized";
          break;
        case "deposited":
          console.log("deposited");
          event.eventName = "deposited";
          event.address = notification[1].value;
          event.assetId = notification[2].value;
          if (notification[3].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[3].value));
          } else if (notification[3].type == "Integer") {
            event.amount = notification[3].value;
          } else {
            event.amount = "Unknown";
          }
          break;
        case "newSingleOrder":
          event.eventName = "singleOrder";
          event.sellerAddress = notification[1].value;
          event.orderHash = notification[2].value;
          event.offerAssetId = notification[3].value;
          if (notification[4].type == "ByteArray") {
            event.offerAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[4].value));
          } else if (notification[4].type == "Integer") {
            event.offerAmount = notification[4].value
          } else {
            event.offerAmount = "Unknown";
          }

          event.wantAssetId = notification[5].value;
          if (notification[6].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[6].value));
          } else if (notification[6].type == "Integer") {
            event.wantAmount = notification[6].value
          } else {
            event.wantAmount = "Unknown";
          }

          event.availableAmount = event.offerAmount;
          event.status = 1;

          console.log("newsingleorder");
          break;
        case "fulfilledSingleOrder":
          console.log("fulfilledSingleOrder");
          event.eventName = "fulfilledSingleOrder";
          event.fillerAddress = notification[1].value;
          event.orderHash = notification[2].value;
          event.offerAssetId = notification[3].value;

          if (notification[4].type == "ByteArray") {
            event.offerAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[4].value));
          } else if (notification[4].type == "Integer") {
            event.offerAmount = notification[4].value
          } else {
            event.offerAmount = "Unknown";
          }

          event.wantAssetId = notification[5].value;

          if (notification[6].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[6].value));
          } else if (notification[6].type == "Integer") {
            event.wantAmount = notification[6].value
          } else {
            event.wantAmount = "Unknown";
          }

          if (notification[7].type == "ByteArray") {
            event.buyAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[7].value));
          } else if (notification[7].type == "Integer") {
            event.buyAmount = notification[7].value
          } else {
            event.buyAmount = "Unknown";
          }
          
          if (notification[8].type == "ByteArray") {
            event.buyForAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[8].value));
          } else if (notification[8].type == "Integer") {
            event.buyForAmount = notification[8].value
          } else {
            event.buyForAmount = "Unknown";
          }

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "singleOrder"
            })
            .toArray(function(err, items) {
              // console.log("items",err, items, event);
              if (items) { // && items.length > 0
                items = items[0];
                console.log("new", items.payload.availableAmount, event.buyAmount);

                let new_available = parseInt(items.payload.availableAmount) - parseInt(event.buyAmount);
                let new_set = {
                  "payload.availableAmount": new_available
                };

                if (new_available <= 0) {
                  new_set["payload.status"] = 0;
                }

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "singleOrder"
                }, {
                  $set: new_set
                });
              }
            });
          break;
        case "cancelledSingleOrder":
          console.log("cancelledSingleOrder");
          event.eventName = "cancelledSingleOrder";
          event.sellerAddress = notification[1].value;
          event.orderHash = notification[2].value;

          new_set = {
            "payload.status": -1
          };
          queues.mongoClient.collections.logs_queue.update({
            "payload.orderHash": event.orderHash,
            "payload.eventName": "singleOrder"
          }, {
            $set: new_set
          });
          break;
        case "newBasket":
          //(OwnerAddress, BasketName, orderHash, status, offerAssetIDs, offerAmounts, wantAssetID, wantAmount)
          event.eventName = "basketOrder";
          event.ownerAddress = notification[1].value;
          event.basketName = notification[2].value;
          event.orderHash = notification[3].value;
          event.orderStatus = parseInt(notification[4].value);
          event.offerAssetIds = notification[5].value.match(/.{1,40}/g);

          basket_amounts = notification[6].value;
          event.offerAmounts = [];

          for (let i = 0; i < basket_amounts.length; i++) {
            if (basket_amounts[i].type == "ByteArray") {
              event.offerAmounts.push(byteArrayToLong(Neon.u.hexstring2ab(basket_amounts[i].value)));
            } else if (basket_amounts[i].type == "Integer") {
              event.offerAmounts.push(basket_amounts[i].value);
            } else {
              event.offerAmounts.push("Unknown");
            }
          }

          event.wantAssetId = notification[7].value;

          if (notification[8].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[8].value));
          } else if (notification[8].type == "Integer") {
            event.wantAmount = notification[8].value;
          } else {
            event.wantAmount = "Unknown";
          }
          console.log("newBasket", event);
          break;
        case "basketModified":
          // (OwnerAddress, orderHash, status, offerAssetIDs, offerAmounts, wantAssetID, wantAmount)
          console.log("basketModified");
          event.eventName = "basketModified";
          event.ownerAddress = notification[1].value;
          event.orderHash = notification[2].value;
          event.orderStatus = parseInt(notification[3].value);
          event.offerAssetIds = notification[4].value.match(/.{1,40}/g);

          basket_amounts = notification[5].value;
          event.offerAmounts = [];

          for (let i = 0; i < basket_amounts.length; i++) {
            if (basket_amounts[i].type == "ByteArray") {
              event.offerAmounts.push(byteArrayToLong(Neon.u.hexstring2ab(basket_amounts[i].value)));
            } else if (basket_amounts[i].type == "Integer") {
              event.offerAmounts.push(basket_amounts[i].value);
            } else {
              event.offerAmounts.push("Unknown");
            }
          }

          event.wantAssetId = notification[6].value;

          if (notification[7].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[7].value));
          } else if (notification[7].type == "Integer") {
            event.wantAmount = notification[7].value;
          } else {
            event.wantAmount = "Unknown";
          }

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "basketOrder"
            })
            .toArray(function(err, items) {
              if (items) {
                items = items[0];
                let new_set = {
                  "payload.offerAssetIds" : event.offerAssetIds,
                  "payload.offerAmounts" : event.offerAmounts
                };

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "basketOrder"
                }, {
                  $set: new_set
                });
              }
            });
          break;
        case "fulfilledBasketOrder":
          // fillerAddress, orderHash, OrderStatus, AssetIds, AssetAmounts, wantAssetId, wantAmount
          event.eventName = "fulfilledBasketOrder";
          event.fillerAddress = notification[1].value;
          event.orderHash = notification[2].value;
          event.orderStatus = parseInt(notification[3].value);
          event.offerAssetIds = notification[4].value.match(/.{1,40}/g);

          basket_amounts = notification[5].value;
          event.offerAmounts = [];

          for (let i = 0; i < basket_amounts.length; i++) {
            if (basket_amounts[i].type == "ByteArray") {
              event.offerAmounts.push(byteArrayToLong(Neon.u.hexstring2ab(basket_amounts[i].value)));
            } else if (basket_amounts[i].type == "Integer") {
              event.offerAmounts.push(basket_amounts[i].value);
            } else {
              event.offerAmounts.push("Unknown");
            }
          }

          event.wantAssetId = notification[6].value;

          if (notification[7].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[7].value));
          } else if (notification[7].type == "Integer") {
            event.wantAmount = notification[7].value
          } else {
            event.wantAmount = "Unknown";
          }

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "basketOrder"
            })
            .toArray(function(err, items) {
              if (items) {
                items = items[0];
                let new_set = {
                  "payload.ownerAddress" : event.fillerAddress,
                  "payload.orderStatus" : event.orderStatus
                };

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "basketOrder"
                }, {
                  $set: new_set
                });
              }
            });
          console.log("fulfilledBasketOrder", event);

          break;
        case "placeBasketOrder":
          // OwnerAddress, orderHash, OrderStatus, AssetIds, AssetAmounts, wantAssetId, wantAmount
          event.eventName = "placeBasketOrder";
          event.ownerAddress = notification[1].value;
          event.orderHash = notification[2].value;
          event.orderStatus = parseInt(notification[3].value);
          event.offerAssetIds = notification[4].value.match(/.{1,40}/g);

          basket_amounts = notification[5].value;
          event.offerAmounts = [];

          for (let i = 0; i < basket_amounts.length; i++) {
            if (basket_amounts[i].type == "ByteArray") {
              event.offerAmounts.push(byteArrayToLong(Neon.u.hexstring2ab(basket_amounts[i].value)));
            } else if (basket_amounts[i].type == "Integer") {
              event.offerAmounts.push(basket_amounts[i].value);
            } else {
              event.offerAmounts.push("Unknown");
            }
          }
          event.wantAssetId = notification[6].value;
          if (notification[7].type == "ByteArray") {
            event.wantAmount = byteArrayToLong(Neon.u.hexstring2ab(notification[7].value));
          } else if (notification[7].type == "Integer") {
            event.wantAmount = notification[7].value
          } else {
            event.wantAmount = "Unknown";
          }

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "basketOrder"
            })
            .toArray(function(err, items) {
              if (items) {
                items = items[0];
                let new_set = {
                  "payload.orderStatus" : event.orderStatus,
                  "payload.wantAssetId" : event.wantAssetId,
                  "payload.wantAmount" : event.wantAmount
                };

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "basketOrder"
                }, {
                  $set: new_set
                });
              }
            });
          console.log("placeBasketOrder", event);
          break;
        case "cancelledBasketOrder":
          // (hash, orderStatus)
          event.eventName = "cancelledBasketOrder";
          event.orderHash = notification[1].value;
          event.orderStatus = parseInt(notification[2].value);

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "basketOrder"
            })
            .toArray(function(err, items) {
              if (items) {
                items = items[0];
                let new_set = {
                  "payload.orderStatus" : event.orderStatus
                };

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "basketOrder"
                }, {
                  $set: new_set
                });
              }
            });
          console.log("cancelledBasketOrder", event);
          break;
        case "liquidatedBasketOrder":
          // (address, orderHash )
          console.log("liquidatedBasketOrder");
          event.eventName = "liquidatedBasketOrder";
          event.ownerAddress = notification[1].value;
          event.orderHash = notification[2].value;

          queues.mongoClient.collections.logs_queue.find({
              "payload.orderHash": event.orderHash,
              "payload.eventName": "basketOrder"
            })
            .toArray(function(err, items) {
              if (items) {
                items = items[0];
                let new_set = {
                  "payload.orderStatus" : -1
                };

                queues.mongoClient.collections.logs_queue.update({
                  "payload.orderHash": event.orderHash,
                  "payload.eventName": "basketOrder"
                }, {
                  $set: new_set
                });
              }
            });
          break;
        case "withdrawMark":
          console.log("withdrawMark");
          event.eventName = "withdrawMark";
          event.withdrawingAddr = notification[1].value;
          event.assetId = notification[2].value;
          if (notification[3].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[3].value));
          } else if (notification[3].type == "Integer") {
            event.amount = notification[3].value
          } else {
            event.amount = "Unknown";
          }
          setMarkDoneLambda(event.withdrawingAddr, event.assetId, event.amount);
          break;
        case "withdrawDone":
          console.log("withdrawDone");
          event.eventName = "withdrawDone";
          event.withdrawingAddr = notification[1].value;
          event.assetId = notification[2].value;
          if (notification[3].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[3].value));
          } else if (notification[3].type == "Integer") {
            event.amount = notification[3].value
          } else {
            event.amount = "Unknown";
          }
          event.utxoUsed = notification[4].value;
          setWithdrawDoneLambda(event.withdrawingAddr, event.assetId, event.amount);
          break;
        case "stakeDeposit":
          console.log("stakeDeposit");
          event.eventName = "stakeDeposit";
          event.address = notification[1].value;
          if (notification[2].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[2].value));
          } else if (notification[2].type == "Integer") {
            event.amount = notification[2].value
          } else {
            event.amount = "Unknown";
          }
          break;
        case "stakewithdraw":
          console.log("stakewithdraw");
          event.eventName = "stakeWithdraw";
          event.address = notification[1].value;
          if (notification[2].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[2].value));
          } else if (notification[2].type == "Integer") {
            event.amount = notification[2].value
          } else {
            event.amount = "Unknown";
          }
          break;
        case "stakeClaim":
          console.log("stakeClaim");
          event.eventName = "stakeClaim";
          event.address = notification[1].value;
          event.assetId = notification[2].value;
          if (notification[3].type == "ByteArray") {
            event.amount = byteArrayToLong(Neon.u.hexstring2ab(notification[3].value));
          } else if (notification[3].type == "Integer") {
            event.amount = notification[3].value
          } else {
            event.amount = "Unknown";
          }
          break;
        case "exchangeStopped":
          console.log("exchangeStopped");
          event.eventName = "exchangeStopped";
          break;
          //EmitExchangeStarted();
          //[DisplayName("exchangeStarted")]
          //public static event Action EmitExchangeStarted;
        case "exchangeStarted":
          console.log("exchangeStarted");
          event.eventName = "exchangeStarted";
          break;

          //DisplayName("exchangeTerminate")]
          //public static event Action EmitExchangeTerminate;
          //EmitExchangeTerminate();
        case "exchangeTerminate":
          console.log("exchangeTerminate");
          event.eventName = "exchangeTerminate";
          break;

          //[DisplayName("addedToWhitelist")]
          //public static event Action<byte[]> EmitAddedToWhitelist; // (scriptHash)
          //EmitAddedToWhitelist(scriptHash);
        case "addedToWhitelist":
          console.log("addedToWhitelist");
          event.eventName = "addedToWhitelist";
          event.scriptHash = notification[1].value;
          break;
          //[DisplayName("removedFromWhitelist")]
          // public static event Action<byte[]> EmitRemovedFromWhitelist; // (scriptHash)
          //EmitRemovedFromWhitelist(scriptHash);

        case "removedFromWhitelist":
          console.log("removedFromWhitelist");
          event.eventName = "removedFromWhitelist";
          event.scriptHash = notification[1].value;
          break;

          // [DisplayName("withdrawerSet")]
          //public static event Action<byte[]> EmitWithdrawerSet; // (address)
          // EmitWithdrawerSet(withdrawerAddress);

        case "withdrawerSet":
          console.log("withdrawerSet")
          event.eventName = "withdrawerSet";
          event.address = notification[1].value;
          break;
        default:
      }
      // console.log("event " , event);
      if (Object.keys(event).length > 0) {
        event.txId = result.txid;
        event.index = index;
        event.block = block;
        interestedLogs.push(event);
        console.log("new event ", event.eventName);
      }
    }
  }
  return interestedLogs;
}

async function logsAnalysis(startBlock, endBlock) {
  let new_logs_queue = queues.logs_queue;
  let latest_block = endBlock
  let latest_block_user = startBlock + 1
  let queueData = []

  if (!endBlock) {
    try {
      response = await getBlockNumber()
      // console.log("block", response);
      latest_block = response
    } catch (err) {
      return done(err)
    }
  }

  let hasNotifications = false;
  let errors = {
    0: [],
    30: [],
    90: []
  }

  let max_loop = 5;
  while (latest_block_user < latest_block && max_loop > 0) {
    try {
      let block = await getTxs(latest_block_user);
      let {
        interestedLogs
      } = await processTransactions(latest_block_user, block)
      //console.log("interestedLogs", interestedLogs);
      queueData = queueData.concat(interestedLogs);
      //console.log("queueData", queueData);
    } catch (err) {
      // log the errors and maybe retry
      if (err !== 'No records found')
        errors[0].push({
          startBlock: latest_block_user,
          confirmation: 0
        })
    }
    latest_block_user++;
    // console.log("latest_block", latest_block_user);
    // latest_block_user = 3487700;
    max_loop--;
  }
  //console.log("queueData", queueData);
  console.log("latest_block_user", latest_block_user);
  for (var i = 0; i < queueData.length; i++) {
    queues.logs_queue.add(queueData[i], function(err, id) {
      // console.log("added", err, id);
    });
    hasNotifications = true;
  }

  return {
    latest_block: --latest_block_user,
    hasNotifications,
    errors
  }
}

module.exports = function(agenda) {
  agenda.define('schedule logs tracking', function(job, done) {
    var userId = job.attrs.data ? job.attrs.data.userId : ''
    console.log("schedule logs tracking");
    agenda.every('16 seconds', 'track new transactions', {
      userId: userId
    });
  })

  agenda.define('track new transactions', function(job, done) {
    // console.log("track new transactions");
    var userId = job.attrs.data ? job.attrs.data.userId : ''
    if (!userId) {
      done({
        error: 'Invalid User Id'
      })
    }
    User.findById(userId)
      .then(async function(_user, error) {
        if (error) throw {
          status: 500,
          json: {
            message: 'Error retrieving user data'
          },
          skip: true
        };
        if (!_user) throw {
          status: 404,
          json: {
            message: 'Not a valid user.'
          },
          skip: true
        };

        let latest_block = 1,
          hasNotifications = false,
          errors = {};

        try {
          let ret = await logsAnalysis(_user.currentBlock)
          if (!ret)
            return done({
              'message': 'Error'
            })

          latest_block = ret.latest_block
          hasNotifications = ret.hasNotifications
          errors = ret.errors
          // latest_block = 165900;
          if (latest_block !== _user.currentBlock)
            await updateUserCurrentBlock(_user._id, latest_block)
        } catch (err) {
          ////console.log(err)
          return done(err)
        }

        // if (hasNotifications) {
        // we can add transactions to queue here ?
        //console.log(hasNotifications);
        // agenda.schedule('10 sec', 'ping client', {userId : userId}, function(err, job) {
        ////console.log('get details of portfolio job posted')
        // })
        //}
        if (errors[0].length || errors[30].length || errors[90].length) {
          // add the block numbers to error queue and retry
          queues.error_tx_queue.add(errors[0])
          queues.error_tx_queue.add(errors[30])
          queues.error_tx_queue.add(errors[90])
          // TODO
          // agenda.now('track transaction for block', {userId : userId}, function(err, job) {
          // 	////console.log('notification ping job posted')
          // })
        }
        done("done");
      })
      .catch(function(error) {
        console.log(error)
        done({
          error: error
        })
      });
  });
  agenda.define('ping client', function(job, done) {
    // is this needed ?
    var userId = job.attrs.data ? job.attrs.data.userId : ''
    queues.logs_queue.get(function(err, msg) {
      console.log("ping", err, msg);
      if (!msg) {
        done({
          error: 'Nothing in queue'
        });
        return;
      }
      var payload = msg.payload
      var p = superagent.put(webhookConfig.PING_URL)
        .send(payload)
        .retry(3)
        .then(function(res) {
          console.log("res", res.body);
          if (res.ok) {
            // console.log("-----------------------------------------", res.body.Data.Code);
            /*
          { AckID: null,
            Code: 'S-001',
              Message: 'Success',
                EventName: 'newSingleOrder' }
					           */
            if (res.body.Code == 'S-001') {
              queues.logs_queue.ack(msg.ack, function(err, id) {
                if (err)
                  console.log(err)
                else
                  console.log('Tx in ping queue with Id :' + id + ' acked')
              })
            }
          } else {
            console.log('Error pinging the webhook')
          }
          done()
        })
        .catch(function(err) {
          console.log(err)
          done()
        });
      // agenda.schedule('10 seconds', 'ping client', {userId : userId});
      // .finally(function() { TODO find better way
      // 	agenda.schedule('5 seconds', 'ping client on new tx', {userId : userId})
      // })
    });
  });
}
