var express = require('express');
var bodyParser = require('body-parser');
var agenda = require('../agenda')
var VerifyToken = require(__root + 'auth/VerifyToken');
var User = require('../models/users');
const queues = require('../queues');

var router = express.Router();
router.use(bodyParser.json());

router.get('/startTrackingTxs', VerifyToken, function(req, res, next) {
  agenda.now('schedule logs tracking', {userId : req.userId}, function(err, job) {
    return res.json({messages : 'Success'});
  })
});

router.post('/setCurrentBlock', VerifyToken, function(req, res, next) {
  let block_number = req.body.block_number;
  if (!block_number || block_number < 0) {
    return res.status(500).send("please send block_number");
  }

  User.findById(req.userId)
  .then (function (_user, error) {
    if (!_user) throw { status: 404, json: { message: 'Not a valid user.' }, skip: true };
    _user.currentBlock = block_number;

    _user.save(function(err, data) {
        console.log("current block ",err, data);
        res.json("block_number: " + block_number);
    });
  })
  .catch (function (error) {
    console.log("catch", error);
    return res.status(500).json(error);
  });
});

router.post('/getOrderDetails', function (req, res) {
  if (!req.body) return res.status(500).send("There was a problem gettting the information from the database.");
  console.log(req.body);
  // console.log(JSON.parse(req.body));

  if (req.body.columns) {
    columns = req.body.columns;
  } else {
    columns = {};
  }

  if (req.body.query) {
    query = req.body.query;
    new_query = {};
    for (i of Object.keys(query)) {
      new_query[`payload.${i}`] = query[i];
    }
  } else {
    query = {};
  }

  if (Object.keys(new_query).length === 0 && Object.keys(new_query).length === 0) {
    return res.status(200).send({"error": "no query" , "data" : []});
  }

  queues.mongoClient.collections.logs_queue.find(new_query, columns)
  .toArray(function(err, items) {
    // console.log(err, items);
    return res.status(200).send({"data" : items});
  });
});

router.post('/getStakingDetails', function (req, res) {
  if (!req.body) return res.status(500).send("There was a problem gettting the information from the database.");
  console.log(req.body);

  if (req.body.columns) {
    columns = req.body.columns;
  } else {
    columns = {};
  }

  if (req.body.query) {
    query = req.body.query;
    new_query = {};
    for (i of Object.keys(query)) {
      new_query[`payload.${i}`] = query[i];
    }
  } else {
    query = {};
  }

  if (Object.keys(new_query).length === 0 && Object.keys(new_query).length === 0) {
    return res.status(200).send({"error": "no query" , "data" : []});
  }

  queues.mongoClient.collections.logs_queue.find(new_query, columns)
  .toArray(function(err, items) {
    // console.log(err, items);
    return res.status(200).send({"data" : items});
  });
});

module.exports = router;
