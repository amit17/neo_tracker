var mongoDbQueue = require('mongodb-queue');

exports.initialize = function(mongoClient) {
  // TODO renthink the visibility time
  // TODO do we need dead-queue?
  // message visibility of 5 mins and a delay to each message of 15 sec.
  // Note : We are using custom mongoqueue with added functionalities
  tx_queue = mongoDbQueue(mongoClient, 'tx_queue', { visibility : 300, delay : 2 });
  logs_queue = mongoDbQueue(mongoClient, 'logs_queue', { visibility : 300, delay : 2 });
  // error_logs_queue = mongoDbQueue(mongoClient, 'logs_queue', { visibility : 300, delay : 5 });
  // tx_ping_queue = mongoDbQueue(mongoClient, 'tx_ping_queue', { visibility : 300, delay : 5 });
  // wallet_queue = mongoDbQueue(mongoClient, 'wallet_queue', { visibility : 300, delay : 5 });
  // hard_wallet_queue = mongoDbQueue(mongoClient, 'hard_wallet_queue', { visibility : 300, delay : 5 });
  // soft_wallet_queue = mongoDbQueue(mongoClient, 'soft_wallet_queue', { visibility : 300, delay : 5 });
  // withdrawal_queue = mongoDbQueue(mongoClient, 'withdrawal_queue', { visibility : 300, delay : 5 });

  // TODO, although this does not create any problem but it doesnt look good to update same index everytime  we start app
  mongoClient.collections.logs_queue.createIndex( {"payload": 1}, {unique:true});
  mongoClient.collections.tx_queue.createIndex( {"payload": 1}, {unique:true});
  // mongoClient.collections.wallet_queue.createIndex( {"payload": 1}, {unique:true});
  // TODO add timestamp to check if its a new request, then we can make payload unique
  // mongoClient.collections.withdrawal_queue.createIndex( {"payload": 1}, {unique:true});

  exports.logs_queue = logs_queue;
  exports.tx_queue = tx_queue;
  exports.mongoClient = mongoClient;
  // exports.error_tx_queue = error_tx_queue;
  // exports.tx_ping_queue = tx_ping_queue;
  // exports.wallet_queue = wallet_queue;
  // exports.withdrawal_queue = withdrawal_queue;
  // exports.hard_wallet_queue = hard_wallet_queue; /* Queue of Hardcoded to_address wallets */
  // exports.soft_wallet_queue = soft_wallet_queue; /* Queue of Variable to_address wallets */
}
