var https = require('https');
var http = require('http');
var fs = require('fs');

// This line is from the Node.js HTTPS documentation.
var options = {
  key: fs.readFileSync('certs/client-key.pem'),
  cert: fs.readFileSync('certs/client-cert.pem')
};

var app = require('./app');
// var port = process.env.PORT || 443;

// Create an HTTP service.
http.createServer(app).listen(80);
// Create an HTTPS service identical to the HTTP service.
https.createServer(options, app).listen(443);

/*var server = app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});*/
